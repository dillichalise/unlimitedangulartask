import { Component, OnInit } from '@angular/core';
import { DeletedEmployeeService } from '../services/deletedEmployee.service';

@Component({
  selector: 'app-trashed-employees',
  templateUrl: './trashed-employees.component.html',
  styleUrls: ['./trashed-employees.component.css']
})
export class TrashedEmployeesComponent implements OnInit {
  deletedEmployeesData: any;

  constructor(public _deletedEmployeeService: DeletedEmployeeService) {
    this.deletedEmployeesData = [];
  }

  ngOnInit() {
    this.listAllDeletedEmployees();
  }

  listAllDeletedEmployees() {
    this._deletedEmployeeService.getDeletedList().subscribe(Response => {
      console.log(Response);
      this.deletedEmployeesData = Response;
    })

  }

}
