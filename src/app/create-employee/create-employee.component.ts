import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { EmployeeService } from '../services/employee.service';
import { Router } from '@angular/router';
import { Employee } from '../models/employee/employee.component';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {
  @ViewChild('employeeForm', { static: false }) public createEmployeeForm: NgForm;
  submitted = false;

  data: Employee = new Employee();
  submitForm: any;
  constructor(
    private _employeeService: EmployeeService,
    private _router: Router
  ) {
  }

  ngOnInit() {
  }

  saveEmployee(empForm: NgForm): void {
    this.submitted = true;
    this._employeeService.addDetails(this.data).subscribe((response) => {
      console.log(this.data);
      this._router.navigate(['list']);
    });
  }

}
