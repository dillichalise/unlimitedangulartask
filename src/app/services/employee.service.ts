import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { Employee } from '../models/employee/employee.component';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  url = 'http://localhost:3000/employees';

  // listURL = 'http://dummy.restapiexample.com/api/v1/employees';
  // createURL = 'http://dummy.restapiexample.com/api/v1/create';
  // deleteURL = 'http://dummy.restapiexample.com/api/v1/delete';
  // updateURL = 'http://dummy.restapiexample.com/api/v1/update';
  // getURL = 'http://dummy.restapiexample.com/api/v1/employee';

  constructor(private http: HttpClient) { }

  //Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  // Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(
      'Something bad happened; please try again later.');
  };


  //Create employee details
  addDetails(details): Observable<Employee> {
    return this.http
      .post<Employee>(this.url, JSON.stringify(details), this.httpOptions)
      .pipe(retry(2), catchError(this.handleError))
  }


  //Get single employee by ID
  getEmployee(id): Observable<Employee> {
    return this.http.get<Employee>(this.url + '/' + id)
      .pipe(retry(2), catchError(this.handleError))
  }

  //Get Employees List
  getList(): Observable<Employee> {
    return this.http.get<Employee>(this.url)
      .pipe(retry(2), catchError(this.handleError))
  }

  //Update Employee Details by ID
  updateEmployeeDetails(id, details): Observable<Employee> {
    return this.http
      .put<Employee>(this.url + '/' + id, JSON.stringify(details), this.httpOptions)
      .pipe(retry(2), catchError(this.handleError))
  }

  //Delete Employee details by ID
  deleteEmployeeDetails(id) {
    return this.http
      .delete<Employee>(this.url + '/' + id, this.httpOptions)
      .pipe(retry(2), catchError(this.handleError))
  }


}
