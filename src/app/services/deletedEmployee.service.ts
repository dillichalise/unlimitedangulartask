import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { Employee } from '../models/employee/employee.component';
import { retry, catchError } from 'rxjs/operators';
import { DeletedEmployee } from '../models/deletedEmployee/deletedEmployee.component';

@Injectable({
    providedIn: 'root'
})
export class DeletedEmployeeService {

    url = 'http://localhost:5000/deletedEmployees';


    constructor(private http: HttpClient) { }

    //Http Options
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    }

    // Handle API errors
    handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            console.error('An error occurred:', error.error.message);
        } else {
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        return throwError(
            'Something bad happened; please try again later.');
    };


    //add deleted details
    addDeletedDetails(details): Observable<Employee> {
        return this.http
            .post<Employee>(this.url, JSON.stringify(details), this.httpOptions)
            .pipe(retry(2), catchError(this.handleError))
    }




    //Get deleted List
    getDeletedList(): Observable<DeletedEmployee> {
        return this.http.get<DeletedEmployee>(this.url)
            .pipe(retry(2), catchError(this.handleError))
    }



}
