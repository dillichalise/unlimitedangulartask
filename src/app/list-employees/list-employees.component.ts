import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { DeletedEmployee } from '../models/deletedEmployee/deletedEmployee.component';
import { Router } from '@angular/router';
import { DeletedEmployeeService } from '../services/deletedEmployee.service';

@Component({
  selector: 'app-list-employees',
  templateUrl: './list-employees.component.html',
  styleUrls: ['./list-employees.component.css']
})
export class ListEmployeesComponent implements OnInit {
  employeesData: any;
  deletedData: DeletedEmployee = new DeletedEmployee();

  constructor(public _employeeService: EmployeeService,
    public _deletedEmployeeService: DeletedEmployeeService,
    _router: Router) {
    this.employeesData = [];
  }

  ngOnInit() {
    this.listAllEmployees();
  }

  listAllEmployees() {
    this._employeeService.getList().subscribe(response => {
      console.log(response);
      this.employeesData = response;
    })
  }

  delete(detail) {
    this._deletedEmployeeService.addDeletedDetails(this.deletedData).subscribe((response) => {
      if (window.confirm('Are you sure want to delete?')) {
        this._employeeService.deleteEmployeeDetails(detail.id).subscribe(response => {
          this.listAllEmployees();
        });
      }
    });


    // setTimeout(() => {

    // },
    //   5000);

  }


}
