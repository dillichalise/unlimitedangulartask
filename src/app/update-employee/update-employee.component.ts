import { Component, OnInit } from '@angular/core';
import { Employee } from '../models/employee/employee.component';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeService } from '../services/employee.service';

@Component({
  selector: 'app-update-employee',
  templateUrl: './update-employee.component.html',
  styleUrls: ['./update-employee.component.css']
})
export class UpdateEmployeeComponent implements OnInit {
  id: number;
  data: Employee;

  constructor(
    public _activatedRoute: ActivatedRoute,
    public _router: Router,
    public _employeeService: EmployeeService
  ) {
    this.data = new Employee();
  }

  ngOnInit() {
    this.id = this._activatedRoute.snapshot.params["id"];
    this._employeeService.getEmployee(this.id).subscribe(response => {
      console.log(response);
      this.data = response;
    })
  }

  update() {
    this._employeeService.updateEmployeeDetails(this.id, this.data).subscribe(response => {
      this._router.navigate(['list']);
    })
  }

}
